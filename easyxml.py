# -*- Mode: Python; tab-width: 4 -*-
#       Id: easyxml.py,v 0.10 2012/06/13 22:20:00
#       Author: Wiktor Rutka <wrutka@gmail.com>

""" Simple xml documents manipulator

This package contains tools and classes for manipulating xml documents.
Using this it is possible to generate and parse documents in xml format.
"""

import sys

XML_DECLARATION_TEMPLATE = '<?xml version="%s" encoding="%s"?>'
XML_ELEMENT_TEMPLATE = '<%s%s>'


class XMLError(Exception):
    """ XMLError
    """
    pass

#TODO: finish parser
#class XMLParser(object):
#    """ xml parser
#    """
#
#    def __init__(self):
#        """ Init XMLParser
#        """
#        pass

#TODO: add finding element
#TODO: adding element in custom place
#TODO: handle xpath
class XMLDocument(object):
    """ An xml basic document

        It handles basic xml doc manipulation
    """

    elements = []
    root_name = ''

    def __init__(self, root, version='0.1', encoding='UTF-8'):
        """ Initialize XMLDocument

            Initialize an xml with proper XML declaration.
            e.g. <?xml version="1.0" encoding="ISO-8859-1"?>
        """
        self.root_name = root
        self._version = version
        self._encoding = encoding

        self.declaration = XML_DECLARATION_TEMPLATE % (version, encoding,)

    # Getters and setters
    @property
    def version(self):
        """ 'version' getter
        """
        return self._version

    @version.setter
    def version(self, version):
        """ 'version' setter

            Invocing this automaticaly updates 'declaration' field
        """
        self._version = version
        self.declaration = XML_DECLARATION_TEMPLATE % (self._version,
                                                      self._encoding,)

    @property
    def encoding(self):
        """ 'encoding' getter
        """
        return self._encoding

    @encoding.setter
    def encoding(self, encoding):
        """ 'encoding' setter

            Invocing this automaticaly updates 'declaration' field
        """
        self._encoding = encoding
        self.declaration = XML_DECLARATION_TEMPLATE % (self._version,
                                                      self._encoding,)

    def write(self, out=sys.stdout,
              separator='\n', indent=''):
        """ Write xml to an output

            All file handler passed to this method should have
            write permissions. Also 'open' and 'close' procedure
            need to be handled outside this method

            Arguments:
                out       -- file handler, can be STDOUT, PIPE
                             or conventional file handler
                separator -- what kind of separator between lines
                indent    -- single line indentation
        """
        out.write(self.string(separator=separator, indent=indent))

    def string(self, separator='\n', indent=0):
        """ Return xml body as a string

            Returned string have 'separator' and indent
            as it is sepcified on input

            Arguments:
                separator -- what kind of separator between lines
                indent    -- single line indentation
        """
        body = self.declaration + separator

        body += '<%s>%s' % (self.root_name, separator)
        for ele in self.elements:
            body += ele.string(separator=separator, indent=indent, baseindent=indent)
        body += '</%s>%s' % (self.root_name, separator)
        return body

    def __unicode__(self):
        """ String projection
        """
        return self.string(separator='')

    def __str__(self):
        """ Old string projection
        """
        return unicode(self).encode('utf-8')

#TODO: make this object iterable
class XMLElement(object):
    """ XML single element

        Contains manipulators and subelements xml single element
    """

    def __init__(self, name, body='', attributes=None, elements=None):
        """ Initialize XMLElement

            Initialise XMLElement with sub-elements: 'elements'

            Arguments:
                name       -- name of the element. Cannot be empty
                attributes -- dict of element's attributes. e.g.:
                              {'id': 'id_element', 'class': 'tabs left-panel'}
                elements   -- instance of XMLElement (as a sub-element)
                              or list of its
        """
        self.sub_eles = []
        self.attributes = {}
        if isinstance(elements, list):
            self.sub_eles.extend(elements)
        elif isinstance(elements, XMLElement):
            self.sub_eles.append(elements)
        elif isinstance(elements, type(None)):
            pass
        else:
            raise XMLError('\'elements\' argument should contain '+\
                           'instance of XMLElement or list of it.')
        if attributes:
            self.attributes.update(attributes)
        self.name = name
        self.body = body

    def set_attributes(self, attrs):
        """ Set element's attributes

            e.g. {'id': 'id_element', 'class': 'tabs left-panel'}
        """
        self.attributes.update(attrs)

    def add_element(self, elements):
        """ Add new sub-elements

            'elements' can be instance of XMLElement
            (as a sub-element) or list of its
            
        """
        if isinstance(elements, list):
            self.sub_eles.extend(elements)
        elif isinstance(elements, XMLElement):
            self.sub_eles.append(elements)
        else:
            raise XMLError('\'elements\' argument should contain '+\
                           'instance of XMLElement or list of it.')

    def remove_element(self, pointer):
        """ Removing element on 'pointer' position
        """
        pass

    def string(self, separator='\n', indent=0, baseindent=0):
        """ returns xml element in str representation
        """
        attrs = ''
        if self.attributes:
            attrs = ' '.join(['%s="%r"' % (att, self.attributes[att])\
                              for att in self.attributes])
            attrs = ' '+attrs

        body = '%s<%s%s>%s' % (' '*indent, self.name, attrs, separator)
        if self.body:
            body += '%s%s%s' % (' '*(indent+baseindent), self.body, separator)
        for ele in self.sub_eles:
            body += ele.string(separator=separator, indent=indent+baseindent, baseindent=baseindent)

        body += '%s</%s>%s' % (' '*indent, self.name, separator)
        return body

    def __unicode__(self):
        """ String projection
        """
        return self.string(separator='')

    def __str__(self):
        """ Old string projection
        """
        return unicode(self).encode('utf-8')

